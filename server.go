package main
import (
	_"bitbucket.org/ranggaaprilio/klipz_dev/db"
	"bitbucket.org/ranggaaprilio/klipz_dev/routes"
	
	"bitbucket.org/ranggaaprilio/klipz_dev/exception"
)



func main() {
	defer exception.Catch()
	
	e := routes.Init()
	e.Logger.Fatal(e.Start(":1323"))
}
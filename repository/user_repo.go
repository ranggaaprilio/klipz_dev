package repository

import (
	"bitbucket.org/ranggaaprilio/klipz_dev/entity"
)

type UserRepository interface {
	Create(value entity.User) (user entity.User, err error)
	FindByEmail(value entity.User, email string) entity.User
	GetAll(users []entity.User) (res []entity.User, err error)
}

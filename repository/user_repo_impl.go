package repository

import (
	"bitbucket.org/ranggaaprilio/klipz_dev/entity"
	"bitbucket.org/ranggaaprilio/klipz_dev/exception"
	"github.com/go-pg/pg/v10"
	// "bitbucket.org/ranggaaprilio/klipz_dev/db"
)

func NewUserRepository(db *pg.DB) *userRepoImpl {
	return &userRepoImpl{
		DB: db,
	}
}

type userRepoImpl struct {
	DB *pg.DB
}

func (repo *userRepoImpl) Create(value entity.User) (entity.User, error) {
	defer repo.DB.Close()
	_, err := repo.DB.Model(&value).Insert()
	if err != nil {
		return value, err
	}
	return value, nil
}

func (repo *userRepoImpl) FindByEmail(value entity.User, email string) (entity.User, error) {
	defer repo.DB.Close()
	err := repo.DB.Model(&value).
		Where("email = ?", email).
		Limit(1).
		Select()

	return value, err
}


func (repo *userRepoImpl) GetAll(users []entity.User) ([]entity.User, error) {
	defer repo.DB.Close()
	err := repo.DB.Model(&users).
		Column("user.*").
		Relation("DetailUser").
		Select()

	if err != nil {
		exception.PanicIfNeeded(err)
		return []entity.User{}, err
	}

	return users,nil
}

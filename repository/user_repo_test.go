package repository

import (
	"fmt"
	"bitbucket.org/ranggaaprilio/klipz_dev/helper"
	"testing"
	"bitbucket.org/ranggaaprilio/klipz_dev/entity"
	"bitbucket.org/ranggaaprilio/klipz_dev/db"
)

func TestUserInsert(t *testing.T){
	userRepo:=NewUserRepository(db.CreateCon())

	salt,err := helper.HashPassword("halo")
	if err != nil {
		panic(err)
	}
	user:=entity.User{
		Name:"rangga Test",
		Email:"rangga@gmail.com",
		Pass:salt,
	}

	result,err:=userRepo.Create(user)
	if err != nil {
		panic(err)
	}

	fmt.Println("result",result)



}

func TestFindByEmail(t *testing.T){
	userRepo:=NewUserRepository(db.CreateCon())

	_,err := helper.HashPassword("halo")
	if err != nil {
		panic(err)
	}
	user:=entity.User{}

	result,err:=userRepo.FindByEmail(user,"rangga@gmail.com")
	if err != nil {
		panic(err)
	}

	fmt.Println("result",result)



}

func TestGetAll(t *testing.T){
	userRepo:=NewUserRepository(db.CreateCon())


	users:=[]entity.User{}

	result,err:=userRepo.GetAll(users)
	if err != nil {
		panic(err)
	}

	fmt.Println("result",result)
	fmt.Println(result[0].Id, result[0].Name ,result[0].DetailUser.Photo)



}
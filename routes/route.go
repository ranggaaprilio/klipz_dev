package routes

import (
	"bitbucket.org/ranggaaprilio/klipz_dev/controller"
	"net/http"

	// "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)


//ServerHeader Config
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderServer, "Aprilio/1.0")
		return next(c)
	}

}

//Init Routing Initialize
func Init() *echo.Echo {
	e := echo.New()
	/** custom Header **/
	e.Use(ServerHeader)

	/** middeleware **/
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	/** Web Routes**/
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Server Is Up And Running")
	})

	/** API Routes **/
	ad := e.Group("/api/v1")
	ad.POST("/login", controller.Login)
	ad.POST("/register", controller.Register)
	ad.POST("/upload", controller.Upload)
	ad.GET("/private_list", controller.GetPrivateHome)
	e.Static("/", "public")

	return e
}
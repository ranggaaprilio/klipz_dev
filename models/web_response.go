package models

type WebResponse struct {
	Code   int         `json:"code"`
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

type AuthResponse struct {
	StatusCode   	int         `json:"statusCode"`
	Message 		string      `json:"message"`
	TokenType		string 		`json:"token_type"`
	AccessToken   	string 		`json:"access_token"`
}

type PrivateListResponse struct {
	Id uint32 `json:id`
	Name string `json:name`
	Photo string `json:pic_photo`
}
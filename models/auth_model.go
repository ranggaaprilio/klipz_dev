package models

type UserRegister struct{
	Name string `json:"name"`
	Email string `json:"email"`
	Password string `json:password`

}

type UserLogin struct{
	Email string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Password string `param:"password" query:"password" form:"password" json:"password" xml:"password"`
}
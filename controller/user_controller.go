package controller

import (
	"bitbucket.org/ranggaaprilio/klipz_dev/db"
	"bitbucket.org/ranggaaprilio/klipz_dev/entity"
	"bitbucket.org/ranggaaprilio/klipz_dev/models"
	"bitbucket.org/ranggaaprilio/klipz_dev/repository"
	"github.com/labstack/echo/v4"
	"net/http"
)

func GetPrivateHome(c echo.Context) error {
	response := new(models.WebResponse)
	userRepo:=repository.NewUserRepository(db.CreateCon())


	users:=[]entity.User{}

	result,err:=userRepo.GetAll(users)

	if err != nil {
		response.Code=http.StatusInternalServerError
		response.Status="false"
		response.Data=err
		return c.JSON(http.StatusInternalServerError, response)
	}

	//fmt.Println(result)
	var res []models.PrivateListResponse

	for _, r := range result {
		listResponse := models.PrivateListResponse{
			Id: r.Id,
			Name: r.Name,
			Photo: r.DetailUser.Photo,
		}

		res=append(
			res,
			listResponse,
		)
	}
	response.Code=http.StatusOK
	response.Status="success"
	response.Data=res
	return c.JSON(http.StatusOK, response)
}

package controller

import (
	"bitbucket.org/ranggaaprilio/klipz_dev/models"
	"github.com/labstack/echo/v4"
	_ "github.com/labstack/echo/v4/middleware"
	"io"
	"net/http"
	"os"
)

func Upload(c echo.Context) error {
	// Read form fields
	response := new(models.WebResponse)
	name := c.FormValue("name")


	//-----------
	// Read file
	//-----------

	// Source
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Destination
	dst, err := os.Create(file.Filename)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	response.Code=200
	response.Status="Success"
	response.Data="upload "+name+" was successfully"

	return c.JSON(http.StatusOK, response)
}
package controller

import (
	"bitbucket.org/ranggaaprilio/klipz_dev/db"
	"bitbucket.org/ranggaaprilio/klipz_dev/repository"
	"log"

	// "encoding/json"
	// "errors"
	// "fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"

	"bitbucket.org/ranggaaprilio/klipz_dev/entity"
	"bitbucket.org/ranggaaprilio/klipz_dev/helper"
	"bitbucket.org/ranggaaprilio/klipz_dev/models"
)


/**
@method:Post
@route:/admin/register
@desc:RegisterUser
**/

func Register(c echo.Context) error {

	response := new(models.WebResponse)
	request := new(models.UserRegister)
	
	
	if err :=c.Bind(&request) ;err!=nil{
		response.Code=http.StatusInternalServerError
		response.Status="false"
		response.Data=err
			return c.JSON(http.StatusInternalServerError, response)
		}
	
	salt,err := helper.HashPassword(request.Password)

	if err!=nil {
		response.Code=http.StatusInternalServerError
		response.Status="false"
		response.Data=err
		return c.JSON(http.StatusInternalServerError, response)
	}

	/**binding to user entity**/
	user:=entity.User{
		Name:request.Name,
		Email:request.Email,
		Pass:salt,
	}
	
	userRepo:=repository.NewUserRepository(db.CreateCon())
	result,err:=userRepo.Create(user)
	if err != nil {
		response.Code=http.StatusInternalServerError
		response.Status="false"
		response.Data=err
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Code=http.StatusOK
	response.Status="success"
	response.Data=result
	return c.JSON(http.StatusOK, response)
}

/**
@method:Post
@route:/admin/login
@desc:login
**/

func Login(c echo.Context) error {

	response := new(models.AuthResponse)
	request := new(models.UserLogin)
	user:=entity.User{}
	if err :=c.Bind(&request) ;err!=nil{
		log.Println(err)
		response.StatusCode=http.StatusInternalServerError
		response.Message= err.Error()
		response.TokenType="null"
		response.AccessToken=""
			return c.JSON(http.StatusInternalServerError, response)
		}

	userRepo:=repository.NewUserRepository(db.CreateCon())
	result,err:=userRepo.FindByEmail(user,request.Email)
	if err != nil {
		response.StatusCode=http.StatusUnauthorized
		response.Message="Unauthorized"
		response.TokenType="null"
		response.AccessToken=""
		return c.JSON(http.StatusUnauthorized, response)
	}

	match := helper.CheckPasswordHash(request.Password, result.Pass)
	if !match{
		response.StatusCode=http.StatusUnauthorized
		response.Message="Unauthorized"
		response.TokenType="null"
		response.AccessToken=""
		return c.JSON(http.StatusUnauthorized, response)
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = user.Id
	claims["name"] = user.Name
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("supersecret"))
	if err != nil {
		response.StatusCode=http.StatusInternalServerError
		response.Message="false"
		response.TokenType="null"
		response.AccessToken=""
		return c.JSON(http.StatusInternalServerError, response)
	}


	response.StatusCode=http.StatusOK
	response.Message="Success"
	response.TokenType="Bearer"
	response.AccessToken=t
	return c.JSON(http.StatusOK, response)
}
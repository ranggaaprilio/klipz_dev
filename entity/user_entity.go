package entity

type User struct{
	Id 			uint32 `pg:",pk"`
	Name 		string
	Email 		string
	Pass 		string
	PhoneNumber string
	DetailUserId uint32
	DetailUser *DetailUser `pg:"rel:has-one"`
}


type DetailUser struct{
	tableName struct{} `pg:"detail_user"`
	Id			uint32
	Phone 		string `pg:"phone_num"`
	Photo 		string
}
--Create Table user

create table users(
	user_id serial not null primary key,
	name varchar(100) not null,
	email varchar(100) not null,
	pass text not null,
	phone_number varchar(15)
);
package db

import(
	"testing"
	// "github.com/stretchr/testify/assert"
	"fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg/v10"
    _"github.com/go-pg/pg/v10/orm"
)

func TestConnection( t *testing.T)  {
	//return dataType *pg.DB
	db := pg.Connect(&pg.Options{
		Addr:     ":5432",
		User:     "rangga",
		Password: "root",
		Database: "klipz_dev",
	})

	ctx := context.Background()

	if err := db.Ping(ctx); err != nil {
		panic(err)
	}
	defer db.Close()

	fmt.Println("Connection successfuly")

}
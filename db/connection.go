package db

import(
	"context"
	"bitbucket.org/ranggaaprilio/klipz_dev/exception"
	"github.com/go-pg/pg/v10"
	// "github.com/go-pg/pg/v10/orm"

)

var db *pg.DB

func CreateCon()*pg.DB{
	db = pg.Connect(&pg.Options{
		Addr:     ":5432",
		User:     "rangga",
		Password: "root",
		Database: "klipz_dev",
	})
	

	ctx := context.Background()

	if err := db.Ping(ctx); err != nil {
		exception.PanicIfNeeded(err	)
	}
	return db
}

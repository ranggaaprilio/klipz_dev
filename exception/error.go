package exception


import(
	"fmt"
)
//PanicIfNeeded for handle error
func PanicIfNeeded(err interface{}) {
	if err != nil {
		panic(err)
		recover()
	}
}

func Catch(){
	if r:=recover();r!=nil{
		fmt.Println("Error occured", r)
		} else {
			fmt.Println("Application running perfectly")
		}
}